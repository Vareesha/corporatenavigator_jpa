package accenture.scp.restservice.facade;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.gson.JsonObject;

import accenture.scp.restservice.util.GsonFactory;

public class Helper {
	
	public static StringBuilder testresturn() {
		StringBuilder sb= new StringBuilder();
		

		try {
			URL url = new URL(
					"https://api10.successfactors.com/odata/v2/Candidate?$format=json&asOfDate=2017-07-29&$filter=candidateId eq '100942'&$select=firstName,lastName,country");
			String encoding = "YWNjZW50dXJlMUBDMDAwMTA4NzY2MlQ6U3lkbmV5MjAxNyE=";

			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.setRequestProperty("Authorization", "Basic " + encoding);
			InputStream content = (InputStream) connection.getInputStream();
			BufferedReader in = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = in.readLine()) != null) {
				System.out.println(line);
				sb.append(line);
			}
			JsonObject jsonObject = GsonFactory.parseGenericJsonObject(sb.toString());
			System.out.println(jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb;

	}

}
