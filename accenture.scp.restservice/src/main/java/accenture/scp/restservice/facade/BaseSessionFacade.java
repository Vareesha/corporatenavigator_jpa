package accenture.scp.restservice.facade;

import java.io.Serializable;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import accenture.scp.restservice.dao.AbstractJpaDao;
import accenture.scp.restservice.util.GsonFactory;


/**
 * 
 * @author jose.c.silva
 *
 */
public abstract class BaseSessionFacade implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3035618272214413384L;
	private final Logger logger = LoggerFactory.getLogger(BaseSessionFacade.class);
	protected final Gson defaultGson = GsonFactory.getInstance().createDefaultGson();

	/**
	 * 
	 */
	public BaseSessionFacade () {
		this.logger.debug("[ENTER] Constructor BaseSessionFacade");
		
		this.logger.debug("[EXIT] Constructor BaseSessionFacade");
	}
	
	/**
	 * 
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	public <T extends AbstractJpaDao<?>> T lookupEJBDao(Class<T> clazz) throws NamingException {
		this.logger.trace("EJB Facade Class name: " + clazz.getSimpleName());

		InitialContext ic = new InitialContext();
		return (T) ic.lookup("java:comp/env/jdbc/DefaultDB");
	}
}

