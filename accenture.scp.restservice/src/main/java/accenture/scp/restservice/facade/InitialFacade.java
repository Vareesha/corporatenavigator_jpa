package accenture.scp.restservice.facade;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import accenture.scp.restservice.dao.InitialDAO;


/**
 * 
 * @author jose.c.silva
 *
 */
@LocalBean
@Stateless
public class InitialFacade extends BaseSessionFacade {
	private static final long serialVersionUID = -34573457395345L;
	private final Logger logger = LoggerFactory.getLogger(InitialFacade.class);
	
	@EJB
	private InitialDAO launchPadDao;

	/**
	 * 
	 */
	public InitialFacade() {
		super();
	}

	
}

