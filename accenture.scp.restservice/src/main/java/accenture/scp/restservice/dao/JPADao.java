package accenture.scp.restservice.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class  JPADao {
	
	private static EntityManagerFactory emFactory;
	

		static {
				try {
					
					emFactory = Persistence.createEntityManagerFactory("jpa-example");
				} catch (Exception e) {
					throw new ExceptionInInitializerError(e);
				}
			} 
	public static EntityManager getEntityManager() {
	    return emFactory.createEntityManager();
	  }

}
