package accenture.scp.restservice.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import accenture.scp.restservice.model.BusinessUnit;
import accenture.scp.restservice.model.KPI;
import accenture.scp.restservice.model.User;

public class DAOQueries {
	EntityManager entityManager = JPADao.getEntityManager();

	public List<User> getUserByNamePwd(String name, String pwd) {

		List<User> userList = entityManager.createNamedQuery("fetchByNamePwd", User.class).setParameter("name", name)
				.setParameter("pwd", pwd).getResultList();

		return userList;

	}

	public List<User> getAllUserNative() {
		@SuppressWarnings("unchecked")
		List<User> userList = entityManager.createNativeQuery("SELECT * FROM \"HCP_RAJ\".\"CORP_LOGIN\"")
				.getResultList();
		return userList;

	}

	public List<User> getAllUserNamed() {
		List<User> userList = entityManager.createNamedQuery("fetchAll", User.class).getResultList();

		return userList;

	}

	public List<Long> procedureCall(String userName, String password) throws Exception {

		Query query = entityManager.createNativeQuery("{call \"HCP_RAJ\".\"RAJProc::Corp_Login1\"(?, ?, ?)}")
				.setParameter(1, userName).setParameter(2, password).setParameter(3, null);

			List<Long> resultList = query.getResultList();
		return resultList;
	}
	
	public List<BusinessUnit> getBusinessUnit() throws Exception {

		List<BusinessUnit> bUnit = (List<BusinessUnit>)entityManager.createNativeQuery("SELECT DISTINCT \"BU_CD\",\"BUSINESS_UNIT\" FROM \"HCP_RAJ\".\"BU_PR_MAP\" ").getResultList();
		return bUnit;
	}
	
	public List<KPI> getKPIValues(String view,String bu,String month,String year) throws Exception {
		

		List<KPI> kpivalues = entityManager.createNativeQuery("SELECT TOP 200 \"KPI_KEY\", \"CC_AP\", \"CC_AP_pcent\", sum(\"CC_ACTUAL\") AS \"CC_ACTUAL\", sum(\"CC_PLANNED\") AS \"CC_PLANNED\", sum(\"CC_FORECAST\") AS \"CC_FORECAST\" FROM \"_SYS_BIC\".\"RAJProc/Overview\"('PLACEHOLDER' = ('$$IP_VIEW$$','"+ view+"'), 'PLACEHOLDER' = ('$$IP_BU$$','"+ bu+"'), 'PLACEHOLDER' = ('$$IP_POPER$$', '"+month+"'), 'PLACEHOLDER' = ('$$IP_GJAHR$$','"+year +"')) GROUP BY \"KPI_KEY\", \"CC_AP\", \"CC_AP_pcent\" ", KPI.class).getResultList();
		return kpivalues;
	}

}
