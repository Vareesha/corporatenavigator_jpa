package accenture.scp.restservice.dao;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import accenture.scp.restservice.model.BusinessUnit;
import accenture.scp.restservice.model.User;

public class ProcedureCallDemo {
	EntityManager entityManager = JPADao.getEntityManager();

	public String getValidInvalidStatus(String param1, String param2) throws SQLException {
		String outParam;

		 entityManager.getTransaction().begin();
		java.sql.Connection connection = entityManager.unwrap(java.sql.Connection.class);
		CallableStatement stmt = connection.prepareCall("{call \"HCP_RAJ\".\"RAJProc::Corp_Login\"(?, ?, ?)}");
		stmt.setString(1, param1);
		stmt.setString(2, param2);
		stmt.registerOutParameter("validate", Types.NVARCHAR);
		stmt.executeUpdate();
		outParam = stmt.getString("validate");
		if (stmt.wasNull()) {
			outParam = null;
		}
		entityManager.getTransaction().commit();
		return outParam;

	}
	public List<User> getuserDetailsProCall() throws SQLException {
		List<User> userList =new ArrayList<User>();

		entityManager.getTransaction().begin();
		java.sql.Connection connection = entityManager.unwrap(java.sql.Connection.class);
		CallableStatement stmt = connection.prepareCall("{CALL \"_SYS_BIC\".\"RAJProc/LOGIN_CALL\"(?)}");
		stmt.execute();
		ResultSet rs = stmt.getResultSet();

		if (rs != null) {
			while(rs.next()){
				User user =new User();
				user.setPwd(rs.getString("PWD"));
				user.setUser_name(rs.getString("USER_NAME"));
				user.setStatus(rs.getString("STATUS"));
				userList.add(user);
			}
		}
		entityManager.getTransaction().commit();
		return userList;

	}
	public List<BusinessUnit> getBusinessUnitDetails() throws SQLException {
		List<BusinessUnit> buList =new ArrayList<BusinessUnit>();

		entityManager.getTransaction().begin();
		java.sql.Connection connection = entityManager.unwrap(java.sql.Connection.class);
		CallableStatement stmt = connection.prepareCall("SELECT DISTINCT \"BU_CD\",\"BUSINESS_UNIT\" FROM \"HCP_RAJ\".\"BU_PR_MAP\" ");
		stmt.execute();
		ResultSet rs = stmt.getResultSet();

		if (rs != null) {
			while(rs.next()){
				BusinessUnit bu =new BusinessUnit();
				bu.setBu_cd(rs.getString("BU_CD"));
				bu.setBusiness_unit(rs.getString("BUSINESS_UNIT"));
				buList.add(bu);
			}
		}
		entityManager.getTransaction().commit();
		return buList;

	}
}
