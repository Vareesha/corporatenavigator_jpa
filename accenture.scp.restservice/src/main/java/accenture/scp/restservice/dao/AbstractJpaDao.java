package accenture.scp.restservice.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import accenture.scp.restservice.util.JPAUtil;




/**
 * 
 * 
 * @author jose.c.silva
 *
 * @param <T>
 */
public abstract class AbstractJpaDao<T extends Serializable> {
	protected Class<T> entityClass;


 	protected EntityManager manager;
	
	public AbstractJpaDao() {
		super();
		//this.manager = JPAUtil.getEntityManager();
	}

	public final void setClazz(final Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public T findOne(final long id) {
		return this.manager.find(entityClass, id);
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() {
		return this.manager.createQuery("from " + this.entityClass.getName() + " e").getResultList();
	}

	public void create(final T entity) {
		this.manager.persist(entity);
	}

	public T update(final T entity) {
		return this.manager.merge(entity);
	}

	public void delete(final T entity) {
		this.manager.remove(entity);
	}

	public void deleteById(final long entityId) {
		final T entity = findOne(entityId);
		delete(entity);
	}
	
}