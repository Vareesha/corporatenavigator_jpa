package accenture.scp.restservice.dao;

import java.util.Calendar;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import accenture.scp.restservice.model.InitialEntity;



/**
 * 
 * @author jose.c.silva
 *
 */
@LocalBean
@Stateless
public class InitialDAO extends AbstractJpaDao<InitialEntity> {
	
	private final Logger logger = LoggerFactory.getLogger(InitialDAO.class);
	
    public InitialDAO() {
        super();
        setClazz(InitialEntity.class);
    }

	    
   

}