package accenture.scp.restservice.service;

import java.io.Serializable;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import accenture.scp.restservice.facade.InitialFacade;

//import com.loblaw.dash.portal.api.util.GsonFactory;
//import com.loblaw.dash.portal.services.common.Gson;



/**
 * 
 * @author jose.c.silva
 *
 */
public abstract class BaseService implements Serializable {

	private static final long serialVersionUID = -60949279807667015L;
	private final Logger logger = LoggerFactory.getLogger(BaseService.class);
	
	//protected final Gson defaultGson = GsonFactory.getInstance().createDefaultGson();
	
	private InitialContext  ctx = null;

	
	@javax.ws.rs.core.Context
	protected HttpServletRequest request; 

	@javax.ws.rs.core.Context
	protected HttpServletResponse response;
	
	/**
	 * 
	 * @param clazz
	 * @return
	 * @throws NamingException
	 */
	@SuppressWarnings("unchecked")
	public <T extends InitialFacade> T lookupJDBCFacade1(Class<T> clazz) throws NamingException {
		this.logger.trace("EJB Facade Class name");

		ctx = new InitialContext();
		return (T) ctx.lookup("java:comp/env/jdbc/DefaultDB");
	}
	

}
