package accenture.scp.restservice.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import accenture.scp.restservice.dao.DAOQueries;
import accenture.scp.restservice.dao.ProcedureCallDemo;
import accenture.scp.restservice.model.BusinessUnit;
import accenture.scp.restservice.model.KPI;
import accenture.scp.restservice.model.LoginResponse;
import accenture.scp.restservice.model.User;

/**
 * @author vareesha.subbanna
 *
 */
public class InitialControllerService extends BaseService {
	private final Logger logger = LoggerFactory.getLogger(InitialControllerService.class);
	private static final long serialVersionUID = -8669079413283714031L;

	DAOQueries dao = new DAOQueries();
	ProcedureCallDemo procedureDemo = new ProcedureCallDemo();
	/*
	 * @EJB private InitialFacade initialFacade;
	 *
	 * private DBManager dbManager;
	 *
	 * private final String[] COLUMN_NAMES = { "COUNTRY_ID", "CONTENT_ID",
	 * "SEQUENCE" };
	 */

	Gson gson = new GsonBuilder().setPrettyPrinting().create();

	/*
	 * public InitialControllerService() throws NamingException,
	 * ServletException { super(); this.initialFacade = new InitialFacade(); }
	 */

	/**
	 *
	 * @return
	 * @throws IOException
	 */
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDetailsByNameAndPwd(@QueryParam("name") String name, @QueryParam("pwd") String pwd) {
		List<User> userList = new ArrayList<User>();
		LoginResponse logResp = new LoginResponse();
		userList = dao.getUserByNamePwd(name, pwd);
		if (userList.size() == 1) {
			logResp.setName(name);
			logResp.setStatus("valid");

		} else {
			logResp.setName(name);
			logResp.setStatus("invalid");
		}

		return Response.status(201).entity(gson.toJson(logResp)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}

	@GET
	@Path("/getAllDetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsersNative() {
		List<User> allUser = dao.getAllUserNative();
		return Response.status(201).entity(gson.toJson(allUser)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}

	@GET
	@Path("/getAllDetailsNative")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUsersNamed() throws Exception {
		List<User> allUser = dao.getAllUserNamed();
		return Response.status(201).entity(gson.toJson(allUser)).build();
	}

	@GET
	@Path("/getStatusFromStoredProc")
	@Produces(MediaType.APPLICATION_JSON)
	public Response storedProcedureCall(@QueryParam("name") String name, @QueryParam("pwd") String pwd)
			throws Exception {
		List<Long> status = dao.procedureCall(name, pwd);
		LoginResponse logResp = new LoginResponse();
		logResp.setName(name);
		for (Long state : status) {
			logResp.setStatusL(state);
		}

		return Response.status(201).entity(gson.toJson(logResp)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();

	}

	@GET
	@Path("/getValidInvalidFromStoredProc")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response checkValidInvalidStatus(@QueryParam("name") String name, @QueryParam("pwd") String pwd)
			throws Exception {

		String outParam = procedureDemo.getValidInvalidStatus(name, pwd);
		LoginResponse logResp = new LoginResponse();

		logResp.setName(name);
		logResp.setStatus(outParam);

		return Response.status(201).entity(gson.toJson(logResp)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();

	}
	@GET
	@Path("/getUsersFromStoredProc")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsersFromPro()
			throws Exception {

		List<User> userList = procedureDemo.getuserDetailsProCall();
		

		return Response.status(201).entity(gson.toJson(userList)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();

	}
	
	@GET
	@Path("/getBusinessUnit")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBusinessUnit() throws Exception {
		List<BusinessUnit> bUnit = dao.getBusinessUnit();
		List<BusinessUnit> bbuDetail = procedureDemo.getBusinessUnitDetails();
		return Response.status(201).entity(gson.toJson(bbuDetail)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}
	@GET
	@Path("/getKPIValues")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getKPIValues(@QueryParam("view") String view,@QueryParam("bu")String bu,
			@QueryParam("month")String month,@QueryParam("year")String year) throws Exception {
		List<KPI> kpiValues = dao.getKPIValues( view, bu, month, year);
		
		return Response.status(201).entity(gson.toJson(kpiValues)).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
				.header("Access-Control-Allow-Headers",
						"X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept,Referer")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true").build();
	}
}
