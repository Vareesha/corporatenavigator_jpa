package accenture.scp.restservice.model;

public class LoginResponse {
	private String name;
	private String status;
	private Long statusL;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getStatusL() {
		return statusL;
	}
	public void setStatusL(Long statusL) {
		this.statusL = statusL;
	}
	
	
	

}
