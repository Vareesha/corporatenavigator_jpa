package accenture.scp.restservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "\"_SYS_BIC\".\"RAJProc/Overview\"")
public class KPI {
	@Id
	@Column(name = "KPI_KEY")
	private String kpiKey;
	private double cc_ap;
	private double cc_ap_pcent;
	private double cc_actual;
	private double cc_plannedx;
	private double cc_forecast;

	public String getKpiKey() {
		return kpiKey;
	}

	public void setKpiKey(String kpiKey) {
		this.kpiKey = kpiKey;
	}

	public double getCc_ap() {
		return cc_ap;
	}

	public void setCc_ap(double cc_ap) {
		this.cc_ap = cc_ap;
	}

	public double getCc_ap_pcent() {
		return cc_ap_pcent;
	}

	public void setCc_ap_pcent(double cc_ap_pcent) {
		this.cc_ap_pcent = cc_ap_pcent;
	}

	public double getCc_actual() {
		return cc_actual;
	}

	public void setCc_actual(double cc_actual) {
		this.cc_actual = cc_actual;
	}

	public double getCc_plannedx() {
		return cc_plannedx;
	}

	public void setCc_plannedx(double cc_plannedx) {
		this.cc_plannedx = cc_plannedx;
	}

	public double getCc_forecast() {
		return cc_forecast;
	}

	public void setCc_forecast(double cc_forecast) {
		this.cc_forecast = cc_forecast;
	}

}
