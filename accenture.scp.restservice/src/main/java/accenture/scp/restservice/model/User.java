package accenture.scp.restservice.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

@Entity
/* @Table(name="\"HCP_VAREESHA\".\"USER\"") */
@Table(name = "\"HCP_RAJ\".\"CORP_LOGIN\"")
@NamedQueries({
		@NamedQuery(name = "fetchByNamePwd", query = "select e from User e where e.user_name=:name and e.pwd=:pwd"),
		@NamedQuery(name = "fetchAll", query = "select u from User u") })
/*@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "procedurecall", procedureName = "\"HCP_RAJ\".\"RAJProc::Corp_Login\"", resultClasses = {
				String.class }, parameters = {
						@StoredProcedureParameter(name = "USERNAME", type = String.class, mode = ParameterMode.IN),
						@StoredProcedureParameter(name = "PASS", type = String.class, mode = ParameterMode.IN),
						@StoredProcedureParameter(name = "OUT_COUNT", type = Integer.class, mode = ParameterMode.OUT) }) })*/
public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * @Id
	 * 
	 * @GeneratedValue private int id;
	 */

	@Id
	private String user_name;

	private String pwd;
	private String status;

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
