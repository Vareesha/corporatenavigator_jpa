package accenture.scp.restservice.util;

public class ProxyRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 5328089118980980522L;

	public ProxyRuntimeException(String message, Throwable th) {
		super(message, th);
	}

}
